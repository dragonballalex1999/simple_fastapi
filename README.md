
# REST API
## Setup
To run this project, install it locally using pip3:

```
$ pip3 install fastapi
$ pip3 install uvicorn
$ pip3 install pydantic
$ pip3 install typing
$ pip3 install starlette
$ pip3 install python-multipart
```

## Run

```

python3 main.py

```

## WEB IU

```

http://127.0.0.1:8000/docs

```

## WEB IU

```

http://127.0.0.1:8000/predict/image

```



