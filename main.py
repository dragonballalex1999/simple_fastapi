from fastapi import FastAPI, UploadFile, File
from starlette.middleware.cors import CORSMiddleware
import uvicorn

app_desc = """<h2>Try this app by uploading any image with `predict/image`</h2>
<h2>Try Covid symptom checker api - it is just a learning app demo</h2>"""

app = FastAPI(title='Tensorflow FastAPI Starter Pack', description=app_desc)


app.add_middleware(
    CORSMiddleware, allow_origins=["*"], allow_methods=["*"], allow_headers=["*"]
)

@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.post("/predict/image")
async def predict_api(file: UploadFile = File(...)):
    extension = file.filename.split(".")[-1] in ("jpg", "jpeg", "png")
    if not extension:
        return "Image must be jpg or png format!"
    #CALL RED
    return "DATA RETURNED"


if __name__ == "__main__":
    uvicorn.run(app, debug=True)