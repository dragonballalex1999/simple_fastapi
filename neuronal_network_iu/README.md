# DJANGO
## Setup
To run this project, install it locally using pip3:

```
$ pip3 install Django
$ sudo pip3 install virtualenv
$ python3 -m virtualenv virtualenv_name   
$ source venv/bin/activate 
$ python3 -m django startproject neuronal_network_iu
$ 
$ cd neuronal_network_iu
$ deactivate
```

## Run

```
python3 manage.py runserver
```