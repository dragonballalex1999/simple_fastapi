from django.apps import AppConfig


class NeuronalNetworkAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'neuronal_network_app'
